import { Injectable } from '@angular/core';

import { Http, Response, URLSearchParams } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {
    constructor(private http: Http) { }

    get(url: string, params: Object = null): Observable<any> {
        let search = params ? new URLSearchParams() : null;
        if (search) { 
            for (var key in params) { 
                search.set(key, params[key]); 
            } 
        }
        
        return this.http.get(environment.api + url, { search }).map(
            (res: Response) => res.json()
            );
    }
    post(url: string, params: Object = null): Observable<any> {
        let body = params ? JSON.stringify(params) : '';
        return this.http.post(environment.api + url, body).map(
            (res: Response) => res.json()
        );
    }
    delete(url: string): Observable<any> {
        return this.http.delete(environment.api + url).map(
            (res: Response) => res.json()
        );
    }


    getData(): Observable<any> {
        var request:Observable<any> = this.get('miurl').share();

        request.subscribe(
            res => { /*console.log('RES en api: ', res.data);*/ },
            err => { console.log('Error en api', err);}
        );

        return request;
    }
}
