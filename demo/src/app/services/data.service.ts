import { Injectable } from '@angular/core';

import { indexOfArrayByKey } from '../shared/utils';

@Injectable()
export class DataService {    

  private persons;
    //constructor(private api: ApiService) { }
    constructor() { 
      this.persons = [
      {id:0, name:"Alberto", surname:"Ordas", age:33},
      {id:1, name:"Alvaro", surname:"Yuste", age:25}, 
      {id:2, name:"Dani", surname:"Nebot", age:34}
      ];
    }

    getPersons() {
      return this.persons;
    }

    getPerson(id) {
      var index = indexOfArrayByKey('id', id, this.persons);       
      return this.persons[index];
    }

    /*getPages() {
        this.api.get(`${pagesPath}`).subscribe(
            res => { this.resetPages(res.data); },
            err => { console.log('Pages could not be fetched', err); }
        );
        return this.pages;
    }
   
    getPage(id = 1) {
        return this.api.get(`${pagesPath}/${id}`);
    }*/
    
}
