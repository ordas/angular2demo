import { Component } from '@angular/core';
//import { NtsPersonListComponent } from './nts-person-list/nts-person-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Nitsnets Demo Angular2';
}
