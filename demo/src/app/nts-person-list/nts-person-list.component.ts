import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import { DataService } from '../services/data.service';


@Component({
  selector: 'app-nts-person-list',
  templateUrl: './nts-person-list.component.html',
  styleUrls: ['./nts-person-list.component.css']
})

export class NtsPersonListComponent implements OnInit {

  private persons = [];


  constructor(
  		private route: ActivatedRoute,
  		private router: Router,
  		private dataService: DataService
  	) { 
  	//console.log("route: " , route.snapshot.data);
  	//console.log("dataService: " , dataService);
  }


  ngOnInit() { 	
  	this.persons = this.dataService.getPersons();
  }

  clickPerson(person) {
  	//console.log('person ', person);
  	this.router.navigate(['/detail', person.id]);
  }



}
