import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
import { DataService } from './services/data.service';
import { ApiService } from './services/api.service';


import { NtsPersonListComponent } from './nts-person-list/nts-person-list.component';
import { NtsPersonDetailComponent } from './nts-person-detail/nts-person-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NtsPersonListComponent,
    NtsPersonDetailComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing    
  ],
  providers: [
    appRoutingProviders,
    DataService,
    ApiService
  ],
  bootstrap: [
    AppComponent
   ]
})
export class AppModule { }
