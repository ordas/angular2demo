import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { AppComponent } from './app.component';
import { NtsPersonListComponent } from './nts-person-list/nts-person-list.component';
import { NtsPersonDetailComponent } from './nts-person-detail/nts-person-detail.component';

const routes: Routes = [
    {
        path: '', component: NtsPersonListComponent, data: {
            title: 'List'
        }
    },
    {
       path: 'detail/:id', component: NtsPersonDetailComponent, data: {
            title: 'Detail'
        }
    },
 
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
