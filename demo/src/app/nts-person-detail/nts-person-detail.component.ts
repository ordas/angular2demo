import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { DataService } from '../services/data.service';
import { ApiService } from '../services/api.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-nts-person-detail',
  templateUrl: './nts-person-detail.component.html',
  styleUrls: ['./nts-person-detail.component.css']
})
export class NtsPersonDetailComponent implements OnInit {
  private person:any;
  private id:number;
  private msg:string;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private dataService: DataService) {
  	//console.log("route: ", this.route.snapshot.params['id']);  	

    this.msg = "cargando...";
  }

  ngOnInit() {  	
  	this.route.params.subscribe(
      params => {      
        this.id = params['id'];
        	//console.log("ID: " + this.id);        	
        }
        );

    //this.onDataOK(this.dataService.getPerson(this.id));

    //console.log("api: " , this.api);
    this.api.getData().subscribe(
        res => { this.onDataOK(res.data) },
        err => { this.onDataKO(err); }
      );
  }

  onDataOK(data:any): void {
    //this.person = this.dataService.getPerson(this.id);
    this.person = data.person;
  }

  onDataKO(err): void {
    //console.log("error: ", err);
    this.msg = "error cargando datos: " + err.statusText;
  }

}
